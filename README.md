# README #

This is a collaborative effort repo for Learning Object 3 in Java EE Course (DT045A). Collaborative meaning that anyone is free to change/improve the code by adding pull requests, forking the repository and do whatever you wish with the code (including selling it to NASA).

The code is based, but separate from the code from earlier Learning Objects https://bitbucket.org/javaee2016/lo1 and https://bitbucket.org/javaee2016/lo2-chocobo

## Requirements ##
* Maven
* PostgreSQL-database running on local machine with a database named jee16 accessible to the user jee16/jee16 (The database will automatically be populated when the server starts)
See https://support.chartio.com/knowledgebase/creating-a-user-with-pgadmin for more info.

* Tomcat 8.5

## Configuration ##

You have to import the project as a Existing Maven project. Don't build an Eclipse project!
If not done already, you have to add the Tomcat server to Eclipse. 

* Go to "Servers" tab
* It will say something like "No servers are available. Click this link to create a new server"
* Then add Tomcat v8.5 server, all settings default
* Add servlet_jsp to the server (should be the only one visible)
* Press 'finish'

Then you have to target a runtime for the servlets_jsp. Do this by right-clicking on project servlets_jsp->Properties. Then find "Targeted Runtimes" and check 'Apache Tomcat v8.5'->Apply

In case there are missing dependencies, you might be able to resolve it by running 'mvn install'

In order for image uploads to work, you need to edit web.xml and set user.imagedir to a readable/writable directory on your computer. This is where images will be uploaded to, and served from.

Now, you should be ready to start the server. Go to the "Servers" tab and right-click->Run

If you have problems with the port 8080 (or any other port) can't be opened, you need to find the offending application. It could be that you have already started Tomcat outside Eclipse, in that case you can open 'Configure Tomcat' and stop the server there, then try to start it from Eclipse. However, there might be many other applications that want to claim port 8080. To find out what process that is using the port, you can type "netstat -abno >netstat.txt" (on Windows) and you'll see the pid associated with the port. Find the pid->process mapping in task manager and kill it.

If you get the problem with inconsistent versions from what is specified in the pom.xml, you might need to clean the offending versions under <workspace>/.metadata/.plugins/[..]. 
