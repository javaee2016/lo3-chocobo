<%@tag description="User image" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@attribute description="User to display image for" required="true" name="user"
	type="se.miun.dsv.javaee16.social.model.User"%>
<%@attribute name="alt" required="false" %>
<c:choose>
	<c:when test="${empty user.imageFileName}">
		<c:url var="userImage" value="/static/no_image.png" />
	</c:when>
	<c:otherwise>
		<c:url var="userImage" value="/user/viewImage/${user.imageFileName}" />
	</c:otherwise>
</c:choose>
<img class="user-image" src="${userImage}" src="${userImage}" alt="${alt}" />
