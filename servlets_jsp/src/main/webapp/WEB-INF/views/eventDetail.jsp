<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib tagdir="/WEB-INF/tags" prefix="t"%>
<t:normalWrapper>
	<jsp:attribute name="pageTitle">
Event Detail
</jsp:attribute>
	<jsp:body>
	<h1>Event Details</h1>
	<%@ include file="eventDetailFragment.jsp"%>
</jsp:body>
</t:normalWrapper>