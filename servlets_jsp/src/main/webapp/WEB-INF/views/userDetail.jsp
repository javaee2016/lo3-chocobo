<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib tagdir="/WEB-INF/tags" prefix="t"%>
<t:normalWrapper>
	<jsp:attribute name="pageTitle">
User Detail
</jsp:attribute>
	<jsp:body>
		<h1>User detail</h1>
		<c:url var="userImageFormTarget" value="/user/uploadImage" />
		<t:userImage user="${user}" />
		<form id="upload-user-image-form" method="POST" action="${userImageFormTarget}" enctype="multipart/form-data">
		<p>
			<input type="file" name="file" id="file" />
			<input type="hidden" name="user-id" value="${user.id}" />
			<input type="submit" value="Upload new image (Max 2MB)" />
			</p>
		</form>
		<h2>${user.firstName} ${user.lastName}</h2>
		<p>
			First name: ${user.firstName}<br />
			Last name: ${user.lastName}<br />
			E-mail: ${user.email}
		</p>
		<h2>Comments by ${user.firstName}</h2>
		<c:choose>
			<c:when test="${!empty user.comments}">
				${user.firstName} has written ${fn:length(user.comments)} comments, here are the latest:
				<c:forEach items="${user.comments}" var="comment">
				<t:comment comment="${comment}" />
				</c:forEach>
			</c:when>
			<c:otherwise>
				${user.firstName} has not written any comments yet.
			</c:otherwise>
		</c:choose>
	</jsp:body>
</t:normalWrapper>