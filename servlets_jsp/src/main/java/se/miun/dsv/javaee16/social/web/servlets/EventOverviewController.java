package se.miun.dsv.javaee16.social.web.servlets;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import se.miun.dsv.javaee16.social.model.Event;
import se.miun.dsv.javaee16.social.service.SocialService;

/**
 * Servlet implementation class EventOverviewController
 */
@WebServlet({"/event/overview", ""})
public class EventOverviewController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private AtomicInteger hits;
	
	@Override
	public void init() {
		hits = new AtomicInteger(0);
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("hits", hits.incrementAndGet());
		SocialService ss = (SocialService) getServletContext().getAttribute("socialService");
		String locationFilter = request.getParameter("filter-string");
		List<Event> events = (locationFilter == null) ? ss.findAllEvents() : ss.findEventsInCity(locationFilter);
		request.setAttribute("events", events);
		RequestDispatcher rd = null;
		String isAjax = request.getParameter("ajax");
		if(isAjax != null && isAjax.equals("true"))
			//Serve only the fragment for an ajax call (an update)
			rd = request.getRequestDispatcher("/WEB-INF/views/eventListFragment.jsp");
		else
			rd = request.getRequestDispatcher("/WEB-INF/views/eventOverview.jsp");
		if(rd == null)
			throw new ServletException("Could not acquire request dispatcher");
		rd.forward(request, response);	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
