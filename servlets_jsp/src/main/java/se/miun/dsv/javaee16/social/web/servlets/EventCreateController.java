package se.miun.dsv.javaee16.social.web.servlets;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import se.miun.dsv.javaee16.social.model.Event;
import se.miun.dsv.javaee16.social.model.User;
import se.miun.dsv.javaee16.social.service.SocialService;

/**
 * Servlet implementation class EventCreateController
 */
@WebServlet("/event/new")
public class EventCreateController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private DateTimeFormatter dateTimeFormatter;
	
	@Override
	public void init() {
		dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SocialService ss = (SocialService) getServletContext().getAttribute("socialService");
		List<User> u = ss.findAllUsers();
		request.setAttribute("availableUsers", u);
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/views/eventCreate.jsp");
		if(rd == null)
			throw new ServletException("Could not acquire request dispatcher");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SocialService ss = (SocialService) getServletContext().getAttribute("socialService");
		Event e = new Event();
		Set<User> availableUsers = null;
		Set<User> organizers = null;
		ArrayList<String> errors = new ArrayList<>();
		try {
			availableUsers = new HashSet<User>(ss.findAllUsers());
			organizers = new HashSet<User>();
			e.setTitle(request.getParameter("title"));
			e.setCity(request.getParameter("city"));
			e.setContent(request.getParameter("content"));

			String startTime = request.getParameter("start-time");
			String endTime = request.getParameter("end-time");

			try {
				if (startTime != null && !startTime.isEmpty()) {
					e.setStartTime(LocalDateTime.parse(request.getParameter("start-time"), dateTimeFormatter));
				}
			} catch(DateTimeParseException ex) {
				errors.add("Invalid start-time date format");
			}

			try {
				if (endTime != null && !endTime.isEmpty()) {
					e.setEndTime(LocalDateTime.parse(request.getParameter("end-time"), dateTimeFormatter));
				}
			} catch(DateTimeParseException ex) {
				errors.add("Invalid end-time date format");
			}
			
			String[] userIds = request.getParameterValues("organizers");

			// Add all organizers to the organizer-set and remove them from the
			// available user set
			// (This should also be possible to do by loading each selected
			// organizer and then subtracting that set
			// from organizers)
			if (userIds != null) {
				boolean oneOrMoreNotFound = false;
				for (String id : userIds) {
					boolean userFound = false;
					for (Iterator<User> it = availableUsers.iterator(); it.hasNext();) {
						User candidate = it.next();
						if (candidate.getId() == Long.parseLong(id)) {
							organizers.add(candidate);
							it.remove();
							userFound = true;
							break;
						}
					}
					if (!userFound) {
						oneOrMoreNotFound = true;
					}
				}
				if(oneOrMoreNotFound) {
					errors.add("One or more organizers were not found in database");
				}
			}
			e.setOrganizers(organizers);
		
			//Try to persist
			if(errors.isEmpty()) {
				ss.addEvent(e);
			}
		} catch(Exception ex) {
			errors.add(ex.getMessage());
		}
		if(!errors.isEmpty()) {
			//failure, show the page again
			request.setAttribute("errors", errors);
			request.setAttribute("event", e);
			if(organizers != null)
				availableUsers.removeAll(organizers);
			request.setAttribute("organizingUsers", e.getOrganizers());
			request.setAttribute("availableUsers", availableUsers);

			RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/views/eventCreate.jsp");
			if(rd == null)
				throw new ServletException("Could not acquire request dispatcher");
			rd.forward(request, response);
			return;
		} else {
			//Success, forward to event overview
			response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/event/overview"));
		}
	}

}
