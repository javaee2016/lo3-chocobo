package se.miun.dsv.javaee16.social.web.servlets;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import se.miun.dsv.javaee16.social.model.User;
import se.miun.dsv.javaee16.social.service.SocialService;
import se.miun.dsv.javaee16.social.web.functions.MimeGuesser;

/**
 * Servlet implementation class UserImageUploadController
 */
@WebServlet(urlPatterns={"/user/uploadImage"}, asyncSupported=true)
@MultipartConfig(maxFileSize=1024*1024*2, maxRequestSize=1024*1024*2)
public class UserImageUploadController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Set<Long> lockedIds; //Used to ensure only one upload per user id at any given moment

	@Override
	public void init() {
		lockedIds = new HashSet<>();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final AsyncContext aContext = request.startAsync();
		aContext.start(new Runnable() {
			public void run() {
				HttpServletRequest aRequest = (HttpServletRequest)aContext.getRequest();
				HttpServletResponse aResponse = (HttpServletResponse )aContext.getResponse();
				String isAjaxStr = aRequest.getParameter("ajax");
				boolean isAjax = isAjaxStr != null && isAjaxStr.equals("true");
				OutputStream os = null;
				InputStream is = null;
				long userId = 0;
				try {
					try {
						userId = Long.parseLong(aRequest.getParameter("user-id"));
					} catch(NumberFormatException e) {
						throw new IllegalArgumentException("Bad user id format", e);
					}
					//Ensure that the user id is not already locked by another upload
					boolean lockAcquired;
					synchronized(lockedIds) {
						lockAcquired = lockedIds.add(userId);
					}
					if(!lockAcquired) {
						throw new RuntimeException("Another image upload for this user is in progress");
					}

					SocialService ss = (SocialService) getServletContext().getAttribute("socialService");
					User u = ss.findUser(userId);
					if(u == null) {
						throw new IllegalArgumentException("User does not exist");
					}

					final Part filePart = aRequest.getPart("file");
					String mime = filePart.getContentType();
					String extension = MimeGuesser.fileExtensionByMime(mime);
					if(extension == null) {
						throw new IllegalArgumentException("Unsupported file format");
					}

					String uploadDir = getServletContext().getInitParameter("user.imagedir");
					if(uploadDir == null) {
						throw new ServletException("User image upload dir not configured");
					}
					//Use create temp file for a guaranteed unique file
					File targetFile = File.createTempFile("userImg-", "." + extension, 
							new File(uploadDir));
					os = new FileOutputStream(targetFile);
					is = new BufferedInputStream(filePart.getInputStream());

					int bytesRead = 0;
					final byte[] buf = new byte[1024];
					while((bytesRead = is.read(buf)) != -1) {
						os.write(buf, 0, bytesRead);
					}

					//Remove old picture, update user image file
					final String oldFileName = u.getImageFileName();

					u.setImageFileName(targetFile.getName());
					ss.saveUser(u);

					if(oldFileName != null && !oldFileName.isEmpty()) {
						if(!(new File(uploadDir + oldFileName).delete())) {
							Logger.getAnonymousLogger().log(Level.WARNING, "Unable to delete old user image " + oldFileName);
						}
					}
					if(isAjax) {
						aResponse.setStatus(HttpServletResponse.SC_OK);
						aResponse.getWriter().append(u.getImageFileName());
					} else {
						aResponse.sendRedirect(aResponse.encodeRedirectURL(aRequest.getContextPath() + "/user/view?id=" + u.getId()));
					}
				} catch(IllegalArgumentException e) {
					sendError(aResponse, HttpServletResponse.SC_BAD_REQUEST, e.getMessage(), isAjax);			
				} catch(IOException | ServletException | RuntimeException e) {
					sendError(aResponse, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage(), isAjax);
				}
				finally {
					synchronized(lockedIds) {
						lockedIds.remove(userId);
					}
					if(is != null)
						try { is.close(); } catch(IOException e) { e.printStackTrace(); }
					if(os != null)
						try { os.close(); } catch(IOException e) { e.printStackTrace(); }
					aContext.complete();
				}
			} //Runnable::run()
		}); //new Runnable()

	}
	
	/**
	 * Helper to send ajax-aware error messages (non-ajax requests are sent the standard tomcat error page, ajax requests get 
	 * only the error message in response body)
	 * @param response
	 * @param code
	 * @param message
	 * @param ajaxResponse
	 */
	private static void sendError(HttpServletResponse response, int code, String message, boolean ajaxResponse) {
		try {
			if(ajaxResponse) {
				response.setStatus(code);
				response.getWriter().append(message);
			} else {
				response.sendError(code, message);
			}
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
}
