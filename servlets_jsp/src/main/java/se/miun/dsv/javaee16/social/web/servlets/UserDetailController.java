package se.miun.dsv.javaee16.social.web.servlets;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import se.miun.dsv.javaee16.social.model.User;
import se.miun.dsv.javaee16.social.service.SocialService;

/**
 * Servlet implementation class UserDetailController
 */
@WebServlet("/user/view")
public class UserDetailController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private AtomicInteger hits;
	
	@Override
	public void init() {
		hits = new AtomicInteger(0);
	}
	

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int userId = 0;
		try {
			userId = Integer.parseInt(request.getParameter("id"));
		} catch(NumberFormatException e) {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return;
		}
		
		SocialService ss = (SocialService) getServletContext().getAttribute("socialService");
		User u = ss.findUser(userId);
		if(u == null) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		}
		
		request.setAttribute("user", u);
		request.setAttribute("hits", hits.incrementAndGet());
		
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/views/userDetail.jsp");
		if(rd == null)
			throw new ServletException("Could not acquire request dispatcher");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
