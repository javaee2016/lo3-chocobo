package se.miun.dsv.javaee16.social.service;

import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import se.miun.dsv.javaee16.social.model.Comment;
import se.miun.dsv.javaee16.social.model.Event;
import se.miun.dsv.javaee16.social.model.User;

public class SocialService {
	private EntityManagerFactory emf;

	public SocialService(EntityManagerFactory emf) {
		this.emf = emf;
	}

	public SocialService() {
		this.emf = Persistence.createEntityManagerFactory("social");
	}

	public void addUser(User u) {
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = null;
		try {
			tx = em.getTransaction();
			tx.begin();
			em.persist(u);
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (tx != null && tx.isActive()) {
				tx.rollback();
			}
			throw e;
		}
	}

	public void addEvent(Event ev) {
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = null;
		try {
			tx = em.getTransaction();
			em.getTransaction();
			tx.begin();
			em.persist(ev);
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (tx != null && tx.isActive()) {
				tx.rollback();
			}
			throw e;
		}
	}

	public void addEventGraph(Set<Event> events, Set<User> users, Set<Comment> comments) {
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = null;
		try {
			tx = em.getTransaction();
			tx.begin();
			for (User u : users) {
				em.persist(u);
			}
			for (Event e : events) {
				em.persist(e);
			}
			for (Comment c : comments) {
				em.persist(c);
			}

			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (tx != null && tx.isActive()) {
				tx.rollback();
			}
			throw e;
		}
	}

	public List<Event> findAllEvents() {
		EntityManager em = emf.createEntityManager();
		return (List<Event>) em.createQuery("SELECT e FROM Event e ORDER BY e.startTime").getResultList();
	}

	public Event findEvent(long eventId) {
		return emf.createEntityManager().find(Event.class, eventId);
	}

	public User findUser(long userId) {
		return emf.createEntityManager().find(User.class, userId);
	}

	public List<Event> findEventsInCity(String cityName) {
		EntityManager em = emf.createEntityManager();
		return (List<Event>) em.createQuery("SELECT e FROM Event e WHERE lower(e.city) LIKE :cityName")
				.setParameter("cityName", (cityName + "%").toLowerCase()).getResultList();
	}

	public void shutDown() {
		if (emf != null && emf.isOpen()) {
			emf.close();
			emf = null;
		}
	}

	public List<User> findAllUsers() {
		EntityManager em = emf.createEntityManager();
		return (List<User>) em.createQuery("SELECT u FROM User u ORDER BY u.lastName").getResultList();
	}

	public void saveUser(User u) {
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = null;
		try {
			tx = em.getTransaction();
			tx.begin();
			em.merge(u);
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (tx != null && tx.isActive()) {
				tx.rollback();
			}
			throw e;
		}
	}

}
