package se.miun.dsv.javaee16.social.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * User of the javaee16 social application
 * @author frni1203
 */
public class User implements Serializable {
	private static final long serialVersionUID = 1L;
	private long id;
	private String firstName;
	private String lastName;
	private String email;
	private String imageFileName;
	private Set<Comment> comments;
	
	public User() {}
	
	/**
	 * Public ctor
	 * @param firstName First name of the user
	 * @param lastName Last name of the user
	 * @param email Email address of the user
	 */
	public User(String firstName, String lastName, String email) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.comments = new HashSet<Comment>();
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", email=" + email + "]";
	}

	public Set<Comment> getComments() {
		return comments;
	}
	
	//Since the schema does not contain Email, I assume that it means to use Email as ID, which is not necessarily suitable
	//for strict adherence to the schema, the email column could be named ID, and forced to be unique, and the surrogate id be called something else
	//for clarity however, I just let email be called Email
	public String getEmail() {
		return email;
	}

	public String getFirstName() {
		return firstName;
	}

	public long getId() {
		return id;
	}

	public String getLastName() {
		return lastName;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@SuppressWarnings("unused")
	private void setId(long id) {
		this.id = id;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getImageFileName() {
		return imageFileName;
	}

	public void setImageFileName(String imageFileName) {
		this.imageFileName = imageFileName;
	}
}
