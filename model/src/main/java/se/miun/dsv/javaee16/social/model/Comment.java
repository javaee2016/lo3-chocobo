package se.miun.dsv.javaee16.social.model;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Comment on an event in the javaee16 social application (Unidirectional relationship with Event, Event owns the relationship)
 * @author frni1203
 */
public class Comment implements Serializable {
	private static final long serialVersionUID = 1L;
	private long id;
	private User author;
	private String commentText;
	private LocalDateTime time;
	private LocalDateTime lastUpdate;
	private Event event;
	
	public Comment() {}
	
	public Comment(Event event, User author, String commentText) {
		this(event, author, commentText, LocalDateTime.now());
	}
	
	public Comment(Event event, User author, String commentText, LocalDateTime timeCreated) {
		this.setAuthor(author);
		this.setEvent(event);
		this.commentText = commentText;
		this.time = timeCreated;	
	}
	
	public User getAuthor() {
		return author;
	}
	
	public String getCommentText() {
		return commentText;
	}
	
	public Event getEvent() {
		return event;
	}
	
	public long getId() {
		return id;
	}

	public LocalDateTime getTimeCreated() {
		return time;
	}

	public LocalDateTime getTimeLastUpdated() {
		return lastUpdate;
	}
	
	public void setAuthor(User author) {
		if(this.author != null) {
			this.author.getComments().remove(this);
		}
		this.author = author;
		if(this.author != null) {
			this.author.getComments().add(this);
		}
	}
	
	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}
	
	public void setEvent(Event event) {
		if(this.event != null) {
			this.event.getComments().remove(this);
		}
		this.event = event;
		if(this.event != null) {
			this.event.getComments().add(this);
		}
	}
	
	@SuppressWarnings("unused")
	private void setId(long id) {
		this.id = id;
	}

	@SuppressWarnings("unused")
	private void setTimeCreated(LocalDateTime timeCreated) {
		this.time = timeCreated;
	}

	@SuppressWarnings("unused")
	private void setTimeLastUpdated(LocalDateTime timeLastUpdated) {
		this.lastUpdate = timeLastUpdated;
	}
	
	@SuppressWarnings("unused")
	private void setCreatedAndLastUpdateTimeNow() {
		if(this.time == null)
			this.time = LocalDateTime.now();
		this.lastUpdate = LocalDateTime.now();
	}
	
	@SuppressWarnings("unused")
	private void setLastUpdateTimeNow() {
		this.lastUpdate = LocalDateTime.now();
	}
}
